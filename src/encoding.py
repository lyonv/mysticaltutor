import re
import encoding_consts as enc
from numpy import NaN
import random
import numpy as np
import math
import numbers

def convert_number(n):
    
    if type(n) is re.Match:
        n = n[0]
    if n is NaN:
        return ""
    if n == "*":
        return enc.token_text_powtough_ast
    if n == "X":
        return enc.token_text_powtough_X
    if type(n) is str:
        n = int(n)
    
    num = []
    for i in range(n):
        num.append(enc.token_number)
    return " ".join(num)

def convert_loyalty_cost(s):
    
    if type(s) is re.Match:
        s = s[0]
    
    text = []
    text.append(enc.token_text_loyaltycoststart)
    
    if s == "0:":
        pass
    else:
        sign = True if (s[0] == "+") else False
        loyalty = int(s[1:-1])
        
        if sign:
            text.append(enc.token_number_pos)
        else:
            text.append(enc.token_number_neg)
        
        text.append(convert_number(loyalty))
    
    text.append(enc.token_text_loyaltycostend)
    text.append(enc.token_text_colon)
    
    return " ".join(text)

def convert_power_toughness(s):
    
    if type(s) is re.Match:
        s = s[0]
    
    text = []
    text.append(enc.token_text_powtoughstart)
    
    power, tough = s.split("/")
    
    text.append(convert_number(power))
    
    text.append(enc.token_text_powtoughmiddle)
    
    text.append(convert_number(tough))
    
    text.append(enc.token_text_powtoughend)
    
    return " ".join(text)

def convert_power_toughness_mod(s):
    
    if type(s) is re.Match:
        s = s[0]
    
    text = []
    text.append(enc.token_text_powtoughmodstart)
    
    power, tough = s.split("/")
    
    sign_power = True if (power[0] == "+") else False
    sign_tough = True if (tough[0] == "+") else False
    power = power[1:]
    tough = tough[1:]
    
    if sign_power:
        text.append(enc.token_number_pos)
    else:
        text.append(enc.token_number_neg)
    
    text.append(convert_number(power))
    
    text.append(enc.token_text_powtoughmodmiddle)
    
    if sign_tough:
        text.append(enc.token_number_pos)
    else:
        text.append(enc.token_number_neg)
    
    text.append(convert_number(tough))
    
    text.append(enc.token_text_powtoughmodend)
    
    return " ".join(text)

def convert_mana(s):
    
    if type(s) is re.Match:
        s = s[0]
    
    mana = []
    
    if s.strip() != "":
    
        mana.append(enc.token_mana_coststart)
        
        # X in mana costs
        regex = re.compile("\{X\}")
        res = re.findall(regex, s)
        for i in range(len(res)):
            mana.append( enc.mana_generic_x )
        
        generic = re.findall(r"\{\d+\}", s)
        if len(generic) > 0:
            num = int( generic[0][1:-1] )
            for i in range(num):
                mana.append( enc.mana_generic )
        
        
        for i, mana_type in enumerate(enc.mana_types):
            regex = re.compile("\{" + mana_type + "\}")
            res = re.findall(regex, s)
            for j in range(len(res)):
                mana.append( enc.mana_tokens[i] )
        
        mana.append(enc.token_mana_costend)
    
    return " ".join(mana)

def convert_rules_text(rt, name):
    rt = rt.strip()
    
    # Replace card name
    rt = re.sub( re.compile( name + "('s)?" ), enc.token_text_cardname, rt )
    
    # Remove reminder text
    rt = re.sub(r"\(.+\)", "", rt)
    
    # Separate abilities by "enter character"
    rt = re.sub("\n", " " + enc.token_text_newline + " ", rt)
    
    # Loyalty ability costs
    rt = re.sub(enc.loyalty_cost_re, convert_loyalty_cost, rt)
    
    # Change commas, dots and colons to tokens
    rt = re.sub(",", " " + enc.token_text_comma, rt)
    rt = re.sub(":", " " + enc.token_text_colon, rt)
    rt = re.sub("\"", " " + enc.token_text_quote + " ", rt)
    rt = re.sub("\.", " " + enc.token_text_dot, rt)
    
    # Symbols: Tap, Untap
    rt = re.sub("{T}", enc.token_text_tap, rt)
    rt = re.sub("{Q}", enc.token_text_untap, rt)
    
    # Energy costs
    rt = re.sub("{E}", enc.token_energy, rt)
    
    # Mana costs
    rt = re.sub(enc.mana_cost_re, convert_mana, rt)
    
    # Power/Toughness and Modifiers
    rt = re.sub(enc.power_toughness_re, convert_power_toughness, rt)
    rt = re.sub(enc.power_toughness_mod_re, convert_power_toughness_mod, rt)
    
    # Arabic numbers
    rt = re.sub("\d+", convert_number, rt)
    
    return rt.upper()

# True if not nan
# False if nan
def nancheck(value):
    return not isinstance(value, numbers.Number) or not math.isnan(value)

# Order
# Cost
# Supertypes
# Types
# Subtypes
# Card Text
# Power
# Toughness
# Loyalty
def encode_card(card):
    n = card["name"].upper() if nancheck(card["name"]) else ""
    m = card["mana_cost"].upper() if nancheck(card["mana_cost"]) else ""
    Sty = card["supertypes"].upper() if nancheck(card["supertypes"]) else ""
    ty = card["types"].upper() if nancheck(card["types"]) else ""
    sty = card["subtypes"].upper() if nancheck(card["subtypes"]) else ""
    ct = card["oracle_text"].strip().upper() if nancheck(card["oracle_text"]) else ""
    p = card["power"]
    t = card["toughness"]
    l = card["loyalty"]
    
    # Start card
    new_card = [enc.token_card_start]
    
    new_card.append( enc.token_sectionid_cost )
    
    # Mana cost
    if re.match( "Land", ty ) is None:
        new_card.append( enc.token_section_necessary )
        new_card.append( convert_mana(m) )
    
    new_card.append( enc.token_section_delimiter )
    
    # Supertype
    new_card.append( enc.token_sectionid_supertype )
    if Sty != "":
        new_card.append( enc.token_section_necessary )
        new_card.append( Sty )
    
    new_card.append( enc.token_section_delimiter )
    
    # Type
    new_card.append( enc.token_sectionid_type )
    new_card.append( enc.token_section_necessary )
    new_card.append( ty )
    
    new_card.append( enc.token_section_delimiter )
    
    # Subtypes
    new_card.append( enc.token_sectionid_subtype )
    if sty != "" or re.match( "Creature", ty ) is not None:
        new_card.append( enc.token_section_necessary )
        new_card.append( sty )
    
    new_card.append( enc.token_section_delimiter )
    
    # Card text
    new_card.append( enc.token_sectionid_text )
    new_card.append( convert_rules_text(ct, n) )
    
    new_card.append( enc.token_section_delimiter )
    
    # Power
    new_card.append( enc.token_sectionid_power )
    if nancheck(p) or re.match( "Creature", ty ) is not None:
        new_card.append( enc.token_section_necessary )
        new_card.append( convert_number(p) )
    
    new_card.append( enc.token_section_delimiter )
    
    # Toughness
    new_card.append( enc.token_sectionid_toughness )
    if nancheck(t) or re.match( "Creature", ty ) is not None:
        new_card.append( enc.token_section_necessary )
        new_card.append( convert_number(t) )
    
    new_card.append( enc.token_section_delimiter )
    
    # Loyalty
    new_card.append( enc.token_sectionid_loyalty )
    if nancheck(l) or re.match( "Planeswalker", ty ) is not None:
        new_card.append( enc.token_section_necessary )
        new_card.append( convert_number(l) )
    
    new_card.append(enc.token_card_end)
    
    return re.sub(r"[ ]+", " ", " ".join(new_card))

def corrupt_card(card_string, corr = 3):
    # remove start and end
    card_string = card_string.replace( enc.token_card_start, "" )
    card_string = card_string.replace( enc.token_card_end, "" )
    
    card_parts = card_string.split(enc.token_section_delimiter)
    
    to_corrupt = random.sample( [x for x in range(len(card_parts))], corr )
    for idx, part in enumerate(card_parts):
        part = part.strip()
        if idx in to_corrupt:
            subparts = part.split(" ")
            card_parts[idx] = " " + subparts[0] + " " #sec id
            if len(subparts) > 1 and subparts[1] == enc.token_section_necessary:
                card_parts[idx] += " " + enc.token_section_necessary + " "
            
    
    new_string = (" " + enc.token_section_delimiter + " ").join(card_parts)
    new_string = enc.token_card_start + " " + new_string + " " + enc.token_card_end
    return re.sub(r"[ ]+", " ", new_string)

def corrupt_list(cards, corr = 3):
    for i, card in enumerate(cards):
        cards[i] = corrupt_card(card, corr)

def encode_dataframe(df):
    res = []
    
    for idx, row in df.iterrows():
        res.append( encode_card(row) )
    
    return res

def get_input_output(df, repetitions = 30, corr = 3):
    out = encode_dataframe(df)
    out = np.repeat(out, repetitions)
    np.random.shuffle(out)
    inp = np.copy(out)
    corrupt_list(inp, corr)
    return inp, out
