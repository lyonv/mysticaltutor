import numpy as np
import encoding_consts as enc

model_dir = "../model/"
model_ext = ".h5"
dict_ext = ".pkl"
model_pre = "mt-"

def names(idi):
    model_name = model_dir + model_pre + idi + model_ext
    encoder_name = model_dir + model_pre + idi + "-e" + model_ext
    decoder_name = model_dir + model_pre + idi + "-d" + model_ext
    dict_name = model_dir + model_pre + idi + dict_ext
    return model_name, encoder_name, decoder_name, dict_name

def create_dictionary(data):
    word_set = set()
    for card in data:
        for word in card.split(" "):
            if word not in word_set:
                word_set.add(word)
    word_list = sorted(list(word_set))
    token_index = dict( [ (word, i+1) for i, word in enumerate(word_list)] )
    return token_index

def encode_input_sequence(data, tokens, max_len):
    input_data = np.zeros((len(data), max_len), dtype='float32')
    
    for i, inp_text in enumerate(data):
        for t, word in enumerate(inp_text.split(" ")):
            input_data[i, t] = tokens[word]
    return input_data

def encode_output_sequence(data, tokens, max_len):
    output_data = np.zeros((len(data), max_len, len(tokens) + 1), dtype='float32')
    
    for i, out_text in enumerate(data):
        for t, word in enumerate(out_text.split(" ")):
            output_data[i, t, tokens[word]] = 1
        for t1 in range(t+1,max_len):
            output_data[i, t1, 0] = 1
    return output_data

def decode_input_sequence(data, inv_tokens):
    result = []
    for card in data:
        c = []
        for num in card:
            if num == 0:
                break
            c.append( inv_tokens[num] )
            if inv_tokens[num] == enc.token_card_end:
                break
        result.append( " ".join(c) )
    return result
        
def decode_output_sequence(data, inv_tokens):
    return decode_input_sequence( np.argmax(data, axis=2), inv_tokens )