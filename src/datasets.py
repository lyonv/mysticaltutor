import pandas as pd
import numpy as np

data_dir = "../data/"
data_file = "scryfall-oracle-cards.csv"
data_path = data_dir + data_file

def load_dataset_default():
	return pd.read_csv(data_path)

def get_dataset_by_set(sets):
	df = load_dataset_default()
	df = df[ np.isin(df["set"], sets) ]
	return df

def get_block_isd():
	return get_dataset_by_set( ["isd", "dka", "avr"] )
def get_block_soi():
	return get_dataset_by_set( ["soi", "emn"] )
def get_block_rav():
	return get_dataset_by_set( ["rav", "gpt", "dis"] )
def get_block_rtr():
	return get_dataset_by_set( ["rtr", "gtc", "dgm"] )
def get_block_grn():
	return get_dataset_by_set( ["grn", "rna", "war"] )
def get_block_zen():
	return get_dataset_by_set( ["zen", "wwk", "roi"] )
def get_block_bfz():
	return get_dataset_by_set( ["bfz", "ogw"] )
def get_block_lrw():
	return get_dataset_by_set( ["lew", "mor", "shm", "eve"] )

def get_plane_isd():
	return pd.concat( [get_block_isd(), get_block_soi()] , ignore_index = True )
def get_plane_rav():
	return pd.concat( [get_block_rav(), get_block_rtr(), get_block_grn()] , ignore_index = True )
def get_plane_zen():
	return pd.concat( [get_block_zen(), get_block_bfz()] , ignore_index = True )

