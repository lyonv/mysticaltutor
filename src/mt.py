# https://medium.com/@dev.elect.iitd/neural-machine-translation-using-word-level-seq2seq-model-47538cba8cd7
# https://towardsdatascience.com/how-to-implement-seq2seq-lstm-model-in-keras-shortcutnlp-6f355f3e5639
# https://keras.io/examples/lstm_seq2seq/

import encoding as encod
import decoding as decod
import datasets as ds
import numpy as np
from mt_utils import *

from keras.layers import Input, LSTM, Embedding, Dense
from keras.models import Model
from keras.utils import plot_model
import pickle

# Configurable
df = ds.get_plane_rav()
model_short = "rav"
model_no = "4"

model_idi = model_short + "-" + model_no

model_name, model_name_e, model_name_d, dict_name = names(model_idi)

inp, out = encod.get_input_output(df, 10)
max_sequence_length = max([ len(card.split(" ")) for card in out ])


# Create dictionary
token_index = create_dictionary(out)
num_tokens = len(token_index)

input_data = encode_input_sequence(inp, token_index, max_sequence_length)
output_data = encode_output_sequence(out, token_index, max_sequence_length)

# Model
embedding_size = max_sequence_length
lstm_layer_size = 256

# Encoder
encoder_inputs = Input(shape=(None, ))
en_x =  Embedding(num_tokens, embedding_size)(encoder_inputs)
encoder1 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 1
encoder_outputs1, state_h1, state_c1 = encoder1(en_x)
encoder_states1 = [state_h1, state_c1]
encoder2 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 2
encoder_outputs2, state_h2, state_c2 = encoder2(encoder_outputs1)
encoder_states2 = [state_h2, state_c2]
encoder3 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 3
encoder_outputs3, state_h3, state_c3 = encoder3(encoder_outputs2)
encoder_states3 = [state_h3, state_c3]

# Decoder
decoder_inputs = Input(shape=(None, ))
de_x = Embedding(num_tokens, embedding_size)(decoder_inputs)
decoder1 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 1
decoder_outputs1, _, _ = decoder1(de_x, initial_state = encoder_states1)
decoder2 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 2
decoder_outputs2, _, _ = decoder2(decoder_outputs1, initial_state = encoder_states2)
decoder3 = LSTM(lstm_layer_size, return_sequences=True, return_state=True) # Layer 3
decoder_outputs3, _, _ = decoder3(decoder_outputs2, initial_state = encoder_states3)

# softmax
decoder_dense = Dense(num_tokens, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs3)

# Compile
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['acc'])

with open(dict_name, 'wb') as f:
        pickle.dump(token_index, f, pickle.HIGHEST_PROTOCOL)

for i in range(0,5):
    # Training
    model.fit([input_data, input_data], output_data, batch_size=128, epochs=100, validation_split=0.20)
    
    # Save model
    model.save(model_name + "." + str((i+1)*100))
    model.save(model_name)

# Predictions
# For now, use the same training data
encoder_output = encoder_states1 + encoder_states2 + encoder_states3
encoder_model = Model(encoder_inputs, encoder_output)
encoder_model.save(model_name_e)

decoder_state_input_h1 = Input(shape=(lstm_layer_size,))
decoder_state_input_c1 = Input(shape=(lstm_layer_size,))
decoder_state_input_h2 = Input(shape=(lstm_layer_size,))
decoder_state_input_c2 = Input(shape=(lstm_layer_size,))
decoder_state_input_h3 = Input(shape=(lstm_layer_size,))
decoder_state_input_c3 = Input(shape=(lstm_layer_size,))
decoder1_states_inputs = [decoder_state_input_h1, decoder_state_input_c1]
decoder2_states_inputs = [decoder_state_input_h2, decoder_state_input_c2]
decoder3_states_inputs = [decoder_state_input_h3, decoder_state_input_c3]
de_x2= Embedding(num_tokens, embedding_size)(decoder_inputs)
decoder_outputsf1, state_hf1, state_cf1 = decoder1(de_x2, initial_state=decoder1_states_inputs)
decoder_outputsf2, state_hf2, state_cf2 = decoder2(decoder_outputsf1, initial_state=decoder2_states_inputs)
decoder_outputsf3, state_hf3, state_cf3 = decoder3(decoder_outputsf2, initial_state=decoder3_states_inputs)
decoder_statesff = [state_hf1, state_cf1, state_hf2, state_cf2, state_hf3, state_cf3]
decoder_outputsff = decoder_dense(decoder_outputsf3)
decoder_model = Model([decoder_inputs] + decoder1_states_inputs + decoder2_states_inputs + decoder3_states_inputs, [decoder_outputsff] + decoder_statesff)
decoder_model.save(model_name_d)


