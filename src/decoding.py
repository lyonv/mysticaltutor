import encoding_consts as enc
import pandas as pd
import re
from numpy import NaN

def decode_number(s):
    if type(s) is re.Match:
        s = s[0]
    if s == enc.token_text_powtough_X:
        return "X"
    if s == enc.token_text_powtough_ast:
        return "*"
    return str(s.count( enc.token_number ))

def decode_loyalty_cost(s):
    if type(s) is re.Match:
        s = s[0]
    s = s.replace(enc.token_text_loyaltycoststart, "").replace(enc.token_text_loyaltycostend, "").strip()
    if s == "":
        return "0"
    parts = s.split(" ")
    res = ""
    if parts[0] == enc.token_number_pos:
        res += "+"
    else:
        res += "-"
    res += decode_number(" ".join(parts[1:]))
    return res

def decode_power_toughness(s):
    if type(s) is re.Match:
        s = s[0]
    s = s.replace(enc.token_text_powtoughstart, "").replace(enc.token_text_powtoughend, "").strip()
    power, toughness = s.split(enc.token_text_powtoughmiddle)
    power = power.strip()
    toughness = toughness.strip()
    return decode_number(power) + "/" + decode_number(toughness)

def decode_power_toughness_modifier(s):
    if type(s) is re.Match:
        s = s[0]
    s = s.replace(enc.token_text_powtoughmodstart, "").replace(enc.token_text_powtoughmodend, "").strip()
    power, toughness = s.split(enc.token_text_powtoughmodmiddle)
    power = power.strip()
    toughness = toughness.strip()
    list_pow = power.split(" ")
    list_tou = toughness.split(" ")
    sign_pow = "+" if list_pow[0] == enc.token_number_pos else "-"
    sign_tou = "+" if list_tou[0] == enc.token_number_pos else "-"
    rest_pow = "" if len(list_pow) == 1 else " ".join(list_pow[1:])
    rest_tou = "" if len(list_tou) == 1 else " ".join(list_tou[1:])
    return sign_pow + decode_number(rest_pow) + "/" + sign_tou + decode_number(rest_tou)

def decode_mana(s):
    if type(s) is re.Match:
        s = s[0]
    s = s.replace(enc.token_mana_coststart, "").replace(enc.token_mana_costend, "").strip()
    if s == "":
        return "{0}"
    res = ""
    
    mana_list = s.split(" ")
    
    # X mana
    for i in range( mana_list.count( enc.mana_generic_x ) ):
        res += "{X}"
    
    # Generic mana
    if s.count( enc.mana_generic ) > 0:
        res += "{" + str(mana_list.count( enc.mana_generic )) + "}"
    
    # All costs
    
    for token, mana in zip(enc.mana_tokens, enc.mana_types):
        for i in range( mana_list.count( token ) ):
            res += "{" + mana + "}"
    
    return res

def decode_rules_text(s):
    s = s.replace(enc.token_text_cardname, "[card name]")
    s = re.sub( re.compile( "[ ]*" + enc.token_text_newline + "[ ]*"), "\n", s)
    s = re.sub( re.compile( "[ ]*" + enc.token_text_comma), ",", s)
    s = re.sub( re.compile( "[ ]*" + enc.token_text_colon), ":", s)
    s = re.sub( re.compile( enc.token_text_quote ), "\"", s)
    s = re.sub( re.compile( "[ ]*" + enc.token_text_dot), ".", s)
    s = s.replace( enc.token_text_tap, "{T}")
    s = s.replace( enc.token_text_untap, "{U}")
    s = s.replace( enc.token_energy, "{E}")
    s = re.sub( enc.coded_mana_re, decode_mana, s )
    s = re.sub(enc.coded_loyalty_re, decode_loyalty_cost, s)
    s = re.sub(enc.coded_powtou_re, decode_power_toughness, s)
    s = re.sub(enc.coded_powtou_mod_re, decode_power_toughness_modifier, s)
    s = re.sub( enc.coded_number_re, decode_number, s )
    return s

def decode_card(s):
    s = s.replace(enc.token_card_start, "").replace(enc.token_card_end, "").strip()
    parts = s.split(enc.token_section_delimiter)
    
    valid = True
    
    card_columns = ["mana_cost", "supertypes", "types", "subtypes", "oracle_text" , "power", "toughness", "loyalty"]
    df = pd.DataFrame(columns = card_columns)
    
    # Cut and assert parts
    if len(parts) == len(enc.card_parts):
        for i in range(len(parts)):
            parts[i] = parts[i].strip().split(" ")
            if parts[i][0] != enc.card_parts[i]:
                valid = False
                break
            parts[i] = " ".join(parts[i][1:]).replace( enc.token_section_necessary, "" )
            parts[i] = re.sub(r"[ ]+", " ", parts[i]).strip()
    else:
        valid = False
    
    if not(valid):
        return df
    
    mana_cost = parts[0]
    print(mana_cost)
    supertypes = parts[1]
    types = parts[2]
    subtypes = parts[3]
    oracle_text = parts[4]
    power = parts[5]
    print(power)
    toughness = parts[6]
    print(toughness)
    loyalty = parts[7]
    
    data = []
    if re.match( "LAND", types ) is None and mana_cost != "":
        data.append(decode_mana(mana_cost))
    else:
        data.append("")
    data.append(supertypes)
    data.append(types)
    data.append(subtypes)
    data.append(decode_rules_text(oracle_text))
    if re.match( "CREATURE", types ) is not None and power != "":
        data.append(decode_number(power))
    else:
        data.append(NaN)
    if re.match( "CREATURE", types ) is not None and toughness != "":
        data.append(decode_number(toughness))
    else:
        data.append(NaN)
    if re.match( "PLANESWALKER", types ) is not None and loyalty != "":
        data.append(decode_number(loyalty))
    else:
        data.append(NaN)
    
    df = df.append( dict(zip(card_columns, data)), ignore_index = True )
    
    return df
    
def decode_dataframe(cards):
    card_columns = ["mana_cost", "supertypes", "types", "subtypes", "oracle_text" , "power", "toughness", "loyalty"]
    df = pd.DataFrame(columns = card_columns)
    
    for card in cards:
        row = decode_card(card)
        df = pd.concat([df, row],ignore_index = True)
    
    return df
