import re

# Encoding tokens
token_card_start = "_CARD_START"
token_card_end = "_CARD_END"

token_section_delimiter = "_SEC_DEL"
token_section_necessary = "_SEC_NECESSARY"

token_sectionid_cost = "_SEC_COST"
token_sectionid_supertype = "_SEC_SUPERTYPE"
token_sectionid_type = "_SEC_TYPE"
token_sectionid_subtype = "_SEC_SUBTYPE"
token_sectionid_text = "_SEC_TEXT"
token_sectionid_power = "_SEC_POWER"
token_sectionid_toughness = "_SEC_TOUGHNESS"
token_sectionid_loyalty = "_SEC_LOYALTY"
card_parts = [token_sectionid_cost, token_sectionid_supertype, token_sectionid_type, token_sectionid_subtype, token_sectionid_text, token_sectionid_power, token_sectionid_toughness, token_sectionid_loyalty]

token_text_newline = "_TEXT_NEWLINE"
token_text_comma = "_TEXT_COMMA"
token_text_colon = "_TEXT_COLON"
token_text_dot = "_TEXT_DOT"
token_text_cardname = "_TEXT_CARDNAME"
token_text_quote = "_TEXT_QUOTE"

token_text_tap = "_TEXT_TAP"
token_text_untap = "_TEXT_UNTAP"

token_energy = "_ENERGY"

token_mana_coststart = "_TEXT_MANACOSTSTART"
token_mana_costend = "_TEXT_MANACOSTEND"

token_text_powtough_X = "_POWTOUGH_X"
token_text_powtough_ast = "_POWTOUGH_AST" #asterisk

token_text_powtoughstart = "_TEXT_POWTOUGHSTART"
token_text_powtoughmiddle = "_TEXT_POWTOUGHMID"
token_text_powtoughend = "_TEXT_POWTOUGHEND"
token_text_powtoughmodstart = "_TEXT_POWTOUGHMODSTART"
token_text_powtoughmodmiddle = "_TEXT_POWTOUGHMODMID"
token_text_powtoughmodend = "_TEXT_POWTOUGHMODEND"

token_text_loyaltycoststart = "_TEXT_LOYALSTART"
token_text_loyaltycostend = "_TEXT_LOYALEND"

mana_generic_x = "_MANA_X"
mana_generic = "_MANA_GENERIC"
mana_colorless = "_MANA_C"
mana_snow = "_MANA_S"
mana_white = "_MANA_W"
mana_blue = "_MANA_U"
mana_black = "_MANA_B"
mana_red = "_MANA_R"
mana_green = "_MANA_G"
mana_hybrid_whiteblue = "_MANA_WU"
mana_hybrid_whiteblack = "_MANA_WB"
mana_hybrid_whitered = "_MANA_WR"
mana_hybrid_whitegreen = "_MANA_WG"
mana_hybrid_blueblack = "_MANA_UB"
mana_hybrid_bluered = "_MANA_UR"
mana_hybrid_bluegreen = "_MANA_UG"
mana_hybrid_blackred = "_MANA_BR"
mana_hybrid_blackgreen = "_MANA_BG"
mana_hybrid_redgreen = "_MANA_RG"
mana_hybrid_twowhite = "_MANA_2W"
mana_hybrid_twoblue = "_MANA_2U"
mana_hybrid_twoblack = "_MANA_2B"
mana_hybrid_twored = "_MANA_2R"
mana_hybrid_twogreen = "_MANA_2G"
mana_phyrexian_white = "_MANA_WP"
mana_phyrexian_blue = "_MANA_UP"
mana_phyrexian_black = "_MANA_BP"
mana_phyrexian_red = "_MANA_RP"
mana_phyrexian_green = "_MANA_GP"

mana_types = [ "C", "S", "W", "U", "B", "R", "G", "W/U", "W/B", "R/W", "G/W", "U/B", "U/R", "G/U", "B/R", "B/G", "R/G", "2/W", "2/U", "2/B", "2/R", "2/G", "W/P", "U/P", "B/P", "R/P", "G/P" ]
mana_tokens = [ mana_colorless, mana_snow, mana_white, mana_blue, mana_black, mana_red, mana_green,mana_hybrid_whiteblue, mana_hybrid_whiteblack, mana_hybrid_whitered, mana_hybrid_whitegreen, mana_hybrid_blueblack, mana_hybrid_bluered, mana_hybrid_bluegreen, mana_hybrid_blackred, mana_hybrid_blackgreen, mana_hybrid_redgreen, mana_hybrid_twowhite, mana_hybrid_twoblue, mana_hybrid_twoblack, mana_hybrid_twored, mana_hybrid_twogreen, mana_phyrexian_white, mana_phyrexian_blue, mana_phyrexian_black, mana_phyrexian_red, mana_phyrexian_green ]

mana_cost_re = re.compile( "(" + "|".join(["\{" + x + "\}" for x in (["X", "\d"] + mana_types) ]) + ")+" )
power_toughness_re = r'((\d)+|X)/((\d)+|X)'
power_toughness_mod_re = r'[+-]((\d)+|X)/[+-]((\d)+|X)'
loyalty_cost_re = r'(0|([+-](\d)+)):'


token_number = "_NUM"
token_number_pos = "_NUM_POS"
token_number_neg = "_NUM_NEG"
token_number_ten = "_NUM_TEN"
token_number_fifteen = "_NUM_FIFTEEN"
token_number_twenty = "_NUM_TWENTY"

coded_posorneg_nre = "(" + token_number_pos + "|" + token_number_neg + ")"
coded_number_nre = token_number + "( " + token_number + ")*"
coded_number_re = re.compile( coded_number_nre )
coded_powtough_number_or_x_nre = "(" + coded_number_nre + "|" + token_text_powtough_X + ")"
coded_number_posneg_nre = coded_posorneg_nre + " " + coded_number_nre
coded_number_posneg_re = re.compile( coded_number_posneg_nre )
coded_number_or_x_posneg_nre = coded_posorneg_nre + " " + coded_powtough_number_or_x_nre
coded_number_or_x_posneg_re = re.compile( coded_number_or_x_posneg_nre )
coded_loyalty_nre =  token_text_loyaltycoststart + " " + "(" + coded_number_or_x_posneg_nre + " )?" + token_text_loyaltycostend
coded_loyalty_re = re.compile( coded_loyalty_nre )
coded_powtou_nre = token_text_powtoughstart + " " + "("+coded_powtough_number_or_x_nre+" )?" + token_text_powtoughmiddle + " " + "("+coded_powtough_number_or_x_nre+" )?" + token_text_powtoughend
coded_powtou_re = re.compile( coded_powtou_nre )
coded_powtou_mod_nre = token_text_powtoughmodstart + " " + coded_posorneg_nre + " " + "("+coded_powtough_number_or_x_nre+" )?" + token_text_powtoughmodmiddle + " " + coded_posorneg_nre + " " + "("+coded_powtough_number_or_x_nre+" )?" + token_text_powtoughmodend
coded_powtou_mod_re = re.compile( coded_powtou_mod_nre )
coded_mana_nre = token_mana_coststart + " " + "(\w* )*" + token_mana_costend
coded_mana_re = re.compile(coded_mana_nre)
