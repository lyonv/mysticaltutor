import datasets as ds
import numpy as np
from mt_utils import *

from keras.layers import Input, LSTM, Embedding, Dense
from keras.models import Model, load_model
from keras.utils import plot_model
import pickle

# Configurable
model_short = "rav"
model_no = "3"

model_idi = model_short + "-" + model_no

model_name, encoder_name, decoder_name, dict_name = names(model_idi)

model = load_model(model_name)
#encoder = load_model(encoder_name)
#decoder = load_model(decoder_name)
with open(dict_name, 'rb') as f:
        token_index = pickle.load(f)

reverse_token_index = dict( [ (i, word) for word, i in token_index.items() ] )

def decode_sequence(input_seq):
    # Encode the input as state vectors.
    states_value = encoder.predict([input_seq])
    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1,1))
    # Populate the first character of target sequence with the start character.
    target_seq[0, 0] = token_index["_CARD_START"]# Sampling loop for a batch of sequences
    # (to simplify, here we assume a batch of size 1).
    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
        output_tokens, h1, c1, h2, c2, h3, c3  = decoder.predict( [target_seq] + states_value )# Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_char = reverse_token_index[sampled_token_index]
        decoded_sentence += ' '+sampled_char# Exit condition: either hit max length
        # or find stop character.
        if (sampled_char == '_CARD_END' or len(decoded_sentence.split(" ")) > 257):
            stop_condition = True # Update the target sequence (of length 1).
        target_seq = np.zeros((1,1))
        target_seq[0, 0] = sampled_token_index# Update states
        states_value = [h1, c1, h2, c2, h3, c3]
    return decoded_sentence

def decode_sequence2(input_seq):
    states_value = encoder.predict(input_seq)
    output_tokens, h1, c1, h2, c2, h3, c3  = decoder.predict( [input_seq] + states_value )
    return decode_output_sequence( output_tokens, reverse_token_index )

def decode_sequence3(input_seq):
    x = model.predict([[input_seq], [input_seq]])
    return decode_output_sequence( x, reverse_token_index )
