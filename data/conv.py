import pandas as pd
import re
import numpy as np

df = pd.read_json("scryfall-oracle-cards.json")
df = df[["id", "name", "mana_cost", "colors", "type_line", "oracle_text" , "power", "toughness", "loyalty", "set", "rarity"]]

# Exclude split cards
df = df[ np.logical_not(df["type_line"].str.contains("//")) ]

#super_types = ["basic" , "elite", "host", "legendary", "ongoing", "snow", "world"]
#types = ["artifact", "conspiracy", "creature", "enchantment", "instant", "land", "phenomenon", "plane", "planeswalker", "scheme", "sorcery", "tribal", "vanguard"]
super_types = ["basic" , "legendary", "snow", "world"]
types = ["artifact", "creature", "enchantment", "instant", "land", "planeswalker", "sorcery", "tribal"]

linetype_regex = "(?P<supertypes>(" + "|".join([s + "\s*" for s in super_types]) + ")*)" + "(?P<types>(" + "|".join([s + "\s*" for s in types]) + " \s)+)" + "[\s—]*" "(?P<subtypes>(\w+\s*)*)"

typ = df["type_line"]
type_res = typ.str.extract(linetype_regex, re.IGNORECASE)

df = pd.concat([df, type_res[["supertypes", "types", "subtypes"]] ], axis=1, join_axes=[df.index], sort=False)
df.drop("type_line", axis = 1, inplace=True)

# Remove weird characters
#df["name"] = df["name"].replace({'[^\w ]':''}, regex=True)
df["oracle_text"] = df["oracle_text"].replace({';':'\n'}, regex=True)
df["oracle_text"] = df["oracle_text"].replace({'[−]+':'-'}, regex=True)


df.dropna(subset=["types"], inplace=True)


df.to_csv("scryfall-oracle-cards.csv")
