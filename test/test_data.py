import sys
sys.path.append('../src')

import pandas as pd
import numpy as np
import encoding_consts as enc
import re

from encoding import *
from mt_test import *
from mt_utils import *

creatures = ["Human", "Elf", "Centaur", "Goblin", "Elephant", "Minotaur", "Vedalken"]
cclass = ["Wizard", "Shaman", "Warrior", "Soldier", "Rogue", "Druid", "Cleric", "Knight"]

ctypes = [x + " " + y for x in creatures for y in cclass]

def make_test_set(data):
    datae = encode_input_sequence(data, token_index, 162)
    result = []
    for d in datae:
    	# Encode
    	res = decode_sequence3(d)
    	result.extend(res)
    return result

def test_set_manacost_1():
	df = pd.read_csv("guildmages.csv", delimiter=";", index_col = 0)
	cards = encode_dataframe(df)
	l = []
	for card_string in cards:
		card_string = card_string.replace( enc.token_card_start, "" )
		card_string = card_string.replace( enc.token_card_end, "" )
		card_parts = card_string.split(enc.token_section_delimiter)
	
		card_parts[0] = enc.token_sectionid_cost + " " + enc.token_section_necessary
		
		new_string = (" " + enc.token_section_delimiter + " ").join(card_parts)
		new_string = re.sub(r"[ ]+", " ",enc.token_card_start + " " + new_string + " " + enc.token_card_end)
		l.append(new_string)
	return l

def test_manacost_1():
    d = test_set_manacost_1()
    return make_test_set(d)

def test_set_creaturetype_1():
	l = []
	
	for t in ctypes:
		new_card = [enc.token_card_start]
		
		new_card.append( enc.token_sectionid_cost )
		new_card.append( enc.token_section_necessary )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_supertype )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_type )
		new_card.append( enc.token_section_necessary )
		new_card.append( "CREATURE" )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_subtype )
		new_card.append( enc.token_section_necessary )
		new_card.append( t.upper() )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_text )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_power )
		new_card.append( enc.token_section_necessary )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_toughness )
		new_card.append( enc.token_section_necessary )
		
		new_card.append( enc.token_section_delimiter )
		
		new_card.append( enc.token_sectionid_loyalty )
		
		new_card.append(enc.token_card_end)
		
		x = re.sub(r"[ ]+", " ", " ".join(new_card))
		l.append(x)
	
	return l
	
def test_creaturetype_1():
    d = test_set_creaturetype_1()
    return make_test_set(d)
